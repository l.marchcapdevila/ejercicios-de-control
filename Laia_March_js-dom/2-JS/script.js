'use strict';

// puntuaciones
const puntuaciones = [
  {
    equipo: 'Toros Negros',
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: 'Amanecer Dorado',
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: 'Águilas Plateadas',
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: 'Leones Carmesí',
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: 'Rosas Azules',
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: 'Mantis Verdes',
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: 'Ciervos Celestes',
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: 'Pavos Reales Coral',
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: 'Orcas Moradas',
    puntos: [2, 3, 3, 4],
  },
];

// # Ejercicio JavaScript 2

// Edita el archivo script.js para crear una función que reciba una array de objetos (equipos y puntos conseguidos) y muestre por consola el ** nombre ** del que más y del que menos puntos hayan conseguido, con sus respectivos ** totales **.

// Encontrarás un array de ejemplo en el propio documento.

//Recorremos el array con un map

const totalScore = puntuaciones.map((score) => {
  const { puntos } = score;

  //Utilizamos el reduce con su acumulador para obtener los puntos totales
  const reduceScore = puntos.reduce((acc, puntos) => acc + puntos);
  score.puntos = reduceScore;

  return score;
});

//Creamos la función para el método sort que nos va a ordenar los resultados.

const maxMinScore = (a, b) => {
  if (a.puntos < b.puntos) {
    return -1;
  }
  if (a.puntos > b.puntos) {
    return 1;
  } else {
    return 0;
  }
};

const orden = totalScore.sort(maxMinScore);

//He usado el pop y el shift para probar métodos que no he usado tanto. A sabiendas que lo elimna del array original.
//Con estos métodos quitamos el último elemento y el primero que en este caso sabemos que son la mayor puntuación y la menor respectivamente.

const firstScore = orden.pop();
const lastScore = orden.shift();
console.log(
  `El equipo con más puntos es ${firstScore.equipo} con ${firstScore.puntos} puntos`
);
console.log(
  `El equipo con menos puntos es ${lastScore.equipo} con ${lastScore.puntos} puntos`
);

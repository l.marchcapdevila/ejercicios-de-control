// # Ejercicio JavaScript 2

// ** Sin modificar el body en el archivo index.html **, utiliza javascript para crear un div en el que se muestre un reloj con la hora actual y que se actualice automáticamente cada segundo.

// El formato del reloj debe ser HH:MM:SS (siempre dos dígitos por cada unidad).

// Si quieres, puedes añadir estilos para que se vea más bonito, pero no se valorará. -->
const divCreate = document.querySelector('body');
const htmlSelector = document.querySelector('html');
divCreate.innerHTML = `<div> </div>`;
const div = document.querySelector('div');

function cero(i) {
  if (i < 10) {
    i = '0' + i;
  }
  return i;
}

setInterval(() => {
  const getNewDate = new Date();

  const hour = cero(getNewDate.getHours());
  const minut = cero(getNewDate.getMinutes());
  const second = cero(getNewDate.getSeconds());

  div.textContent = `${hour}:${minut}:${second}`;
}, 1000);

//añadimos CSS al elemento
div.style.fontSize = '50px';
div.style.color = 'white';
htmlSelector.style.background = 'lightsalmon';

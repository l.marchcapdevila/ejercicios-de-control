// # Ejercicio JavaScript

// Edita el archivo script.js para crear una función que haga lo siguiente:

// - Generar una contraseña (número entero aleatorio del 0 al 100)
// - Pedir al usuario que introduzca un número dentro de ese rango.
// - Si el número introducido es igual a la contraseña, aparecerá en pantalla un mensaje indicando que ha ganado; si no, el mensaje indicará si la contraseña en un número mayor o menor al introducido y dará una nueva oportunidad.
// - El usuario tendrá 5 oportunidades de acertar, si no lo consigue, aparecerá un mensaje indicando que ha perdido.

const password = Math.floor(Math.random() * 101);

function getPassword() {
  for (let i = 5; i > 0; i--) {
    const mensaje = Number(prompt('Introduce un número entre 0 y 100'));

    const contador = i - 1;
    if (mensaje === password) {
      alert(`Enhorabuena, has ganado!! El número era el ${password}`);
      break;
    } else if (mensaje > password) {
      alert(`La contraseña es un número menor, te quedan ${contador} intentos`);
    } else if (mensaje < password) {
      alert(`La contraseña es un número mayor, te quedan ${contador} intentos`);
    }
    if (mensaje !== password && contador === 0) {
      alert(`Lo siento, has perdido, la contraseña era: ${password}`);
    }
  }
}

getPassword();

console.log(password);
